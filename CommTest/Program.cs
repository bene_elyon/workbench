﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace CommTest
{
    class Program
    {
        static void Pause()
        {
            Console.WriteLine( "Hit Enter to continue...." );
            Console.ReadLine();
        }

        static void Main( string[] args )
        {
            try
            {
                IPHostEntry ipHostInfo = Dns.GetHostEntry( Dns.GetHostName() );

                foreach ( IPAddress ip in ipHostInfo.AddressList )
                {
                    Console.WriteLine( $"{ ip.ToString() } - { ip.AddressFamily }" );
                }

                //IPAddress ipAddress = ipHostInfo.AddressList[0];

                Socket listenter = new Socket( AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp );
                listenter.Bind( new IPEndPoint( IPAddress.Parse( "192.168.134.1" ), 6800 ) );
                listenter.Listen( 0 );

                Socket newSocket = listenter.Accept();
                Console.WriteLine( newSocket.RemoteEndPoint.ToString() + " - " + newSocket.Connected );

                Task.Run( () => { Thread.Sleep( 1500 ); newSocket.Close(); } );

                try
                {
                    byte[] bytes = new byte[256];
                    int bytesRcvd = newSocket.Receive( bytes );
                    if ( bytesRcvd == 0 )
                    {
                        Console.WriteLine( "Closed[0]!" );
                    }
                }
                catch ( SocketException )
                { Console.WriteLine( "Closed[ex]!" ); }

                Pause();

                if ( newSocket.Connected )
                { Console.WriteLine( newSocket.RemoteEndPoint.ToString() + " - " + newSocket.Connected ); }
                else
                { Console.WriteLine( "NOT connected!" ); }
            }
            catch ( Exception ex )
            {
                Console.WriteLine( ex.ToString() );
            }

            Pause();
        }
    }
}

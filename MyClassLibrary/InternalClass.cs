﻿using System;

namespace MyClassLibrary
{
    internal class InternalClass : IMyInterface
    {
        public InternalClass()
        { }

        public void Test()
        {
            Console.WriteLine( "Hi from InternalClass" );
        }
    }
}

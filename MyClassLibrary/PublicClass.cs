﻿using System;

namespace MyClassLibrary
{
    public class PublicClass
    {
        private InternalClass m_internalClass = new InternalClass();

        public PublicClass()
        { }

        public IMyInterface Get()
        {
            return m_internalClass;
        }
    }
}

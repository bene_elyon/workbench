﻿using System;

namespace MyClassLibrary
{
    public class TestClassBase
    {
        public string PublicProp { get; set; } = "Hidden";

        internal string InternalProp { get; set; } = "Hidden";

        protected string ProtectedProp { get; set; } = "Hidden";

        private string PrivateProp { get; set; } = "Hidden";

        protected internal string ProtectedInternalProp { get; set; } = "Hidden";

        private protected string PrivateProtectedProp { get; set; } = "Hidden";

        public TestClassBase()
        { }

        /// <summary>
        /// 
        /// </summary>
        public void PublicMethod( bool isVisible = false )
        {
            Console.WriteLine( "Public Method is " + ( isVisible ? "visible" : "hidden" ) );
        }

        /// <summary>
        /// 
        /// </summary>
        protected void ProtectedMethod( bool isVisible = false )
        {
            Console.WriteLine( "Protected Method is " + ( isVisible ? "visible" : "hidden" ) );
        }

        /// <summary>
        /// 
        /// </summary>
        internal void InternalMethod( bool isVisible = false )
        {
            Console.WriteLine( "Internal Method is " + ( isVisible ? "visible" : "hidden" ) );
        }

        /// <summary>
        /// 
        /// </summary>
        private void PrivateMethod( bool isVisible = false )
        {
            Console.WriteLine( "Private Method is " + ( isVisible ? "visible" : "hidden" ) );
        }

        /// <summary>
        /// 
        /// </summary>
        protected internal void ProtectedInternalMethod( bool isVisible = false )
        {
            Console.WriteLine( "Protected Internal Method is " + ( isVisible ? "visible" : "hidden" ) );
        }

        /// <summary>
        /// Only in .NET 7.2 and above.
        /// </summary>
        private protected void PrivateProtectedMethod( bool isVisible = false )
        {
            Console.WriteLine( "Protected Internal Method is " + ( isVisible ? "visible" : "hidden" ) );
        }
    }
}

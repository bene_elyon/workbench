﻿using System;



namespace MyClassLibrary
{
    public class TestClassInternalDerived : TestClassBase
    { 


        public TestClassInternalDerived()
        { }

        public void Test()
        {
            PublicProp = "Visible";
            PublicMethod();                 // "Visible"

            InternalProp = "Visible";
            InternalMethod();               // "Visible"

            ProtectedProp = "Visible";
            ProtectedMethod();              // "Visible"

            //PrivateProp = "Hidden";
            //PrivateMethod();              // "Hidden"

            ProtectedInternalProp = "Visible";
            ProtectedInternalMethod();      // "Visible"

            // Only in .NET 7.2 and above.
            PrivateProtectedProp = "Visible";
            PrivateProtectedMethod();       // "Visible"
        }
    }
}

﻿using System;



namespace MyClassLibrary
{
    public class XAnotherClass
    {
        private TestClassBase MyTestClass = new TestClassBase();

        public XAnotherClass()
        {
            PublicClass publicClass = new PublicClass();        //"Visible"
            InternalClass internalClass = new InternalClass();  //"Visible"
        }

        public void Test()
        {
            MyTestClass.PublicProp = "Visible";
            MyTestClass.PublicMethod();                     //"Visible"

            MyTestClass.InternalProp = "Visible";
            MyTestClass.InternalMethod();                   // "Visible"

            //MyTestClass.ProtectedProp = "Hidden";
            //MyTestClass.ProtectedMethod();                // "Hidden"

            //MyTestClass.PrivateProp = "Hidden"; 
            //MyTestClass.PrivateMethod();                  // "Hidden"

            MyTestClass.ProtectedInternalProp = "Visible";
            MyTestClass.ProtectedInternalMethod();          // "Visible"

            // Only in .NET 7.2 and above.
            //MyTestClass.PrivateProtectedProp = "Hidden";
            //MyTestClass.PrivateProtectedMethod();         // "Hidden"
        }
    }
}

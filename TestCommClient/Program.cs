﻿using System;
using System.Net;
using System.Net.Sockets;

namespace TestCommClient
{
    class Program
    {
        static void Pause()
        {
            Console.WriteLine( "Hit Enter to continue..." );
            Console.ReadLine();
        }

        static void Main( string[] args )
        {
            try
            {
                Socket socket = new Socket( AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp );
                socket.Connect( IPAddress.Parse( "192.168.134.1" ), 6800 );

                Console.WriteLine( "Connected!" );
                Console.WriteLine( socket.LocalEndPoint.ToString() + " -> " + socket.RemoteEndPoint.ToString() );
                Pause();
                socket.Close();
            }
            catch ( Exception ex )
            {
                Console.WriteLine( ex.ToString() );
            }

            Pause();
        }
    }
}

﻿using System;

using MyClassLibrary;

namespace TestConsole
{
    class Program
    {
        static void Pause()
        {
            Console.WriteLine( "Hit Enter to continue..." );
            Console.ReadLine();
        }

        static void Main( string[] args )
        {
            try
            {
                ProgramClass prog = new ProgramClass();

                prog.Test();

                PublicClass publicClass = new PublicClass();

                publicClass.Get().Test();

                object obj = "Test This";
                object o = new string( "Test This".ToCharArray() );

                Console.WriteLine( obj == o );
                Console.WriteLine( obj.Equals( o ) );
            }
            catch ( Exception ex )
            {
                Console.WriteLine( ex.ToString() );
            }

            Pause();
        }
    }
}

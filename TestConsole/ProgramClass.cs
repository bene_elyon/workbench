﻿using System;

using MyClassLibrary;

namespace TestConsole
{
    public class ProgramClass
    {
        TestClassBase testClass = new TestClassBase();

        TestClassInternalDerived testClassInteralDerived = new TestClassInternalDerived();

        TestClassExternalDerived testClassExternalDerived = new TestClassExternalDerived();

        public ProgramClass()
        {
            PublicClass publicClass = new PublicClass();          // "Visible"
            //InternalClass internalClass = new InternalClass();  // "Hidden"
        }

        public void Test()
        {
            testClass.PublicProp = "Visible";
            testClass.PublicMethod( true );                   // "Visible"

            testClassInteralDerived.PublicProp = "Visible";
            testClassInteralDerived.PublicMethod( true );     // "Visible"

            testClassExternalDerived.PublicProp = "Visible";
            testClassExternalDerived.PublicMethod( true );    // "Visible"
        }
    }
}

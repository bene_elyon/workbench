﻿using System;

using MyClassLibrary;

namespace TestConsole
{
    public class TestClassExternalDerived : TestClassBase
    {
        public TestClassExternalDerived()
        { }

        public void Test()
        {
            PublicProp = "Visible";
            PublicMethod();                 // "Visible"

            //InternalProp = "Hidden";
            //InternalMethod();             // "Hidden"

            ProtectedProp = "Visible";
            ProtectedMethod();              // "Visible"

            //PrivateProp = "Hidden";
            //PrivateMethod();              // "Hidden"

            ProtectedInternalProp = "Visible";
            ProtectedInternalMethod();      // "Visible"

            // Only in .NET 7.2 and above.
            //PrivateProtectedProp = "Hidden";
            //PrivateProtectedMethod();       // "Hidden"
        }
    }
}
